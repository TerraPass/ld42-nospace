﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRandomGen : MonoBehaviour {

    private int MapH;
    private int MapW;
    private int[,] MapGrid;

    private string DebugStr;

    // Use this for initialization
    void Start () {
        MapH = Random.Range(6, 20);
        MapW = Random.Range(6, 20);
        MapGrid = new int[MapH, MapW];

        Debug.Log(MapH);
        Debug.Log(MapW);

        //FillRand();
        FillOnes();
        //Decimate();
        //Decimate();
        RandWorms();

        FillZeros();
        CreateEntrance();


        ConsoleDisplay();
    }

    void FillOnes()
    {
        for (int i = 0; i < MapH; i++)
        {
            for (int j = 0; j < MapW; j++)
            {
                MapGrid[i, j] = 1;
            }
        }
    }
    void FillRand()
    {
        for (int i=0; i < MapH; i++)
        {
            for (int j = 0; j < MapW; j++)
            {
                MapGrid[i, j] = Random.Range(0, 2);
            }
        }
    }

    void FillZeros()
    {
        for (int i = 0; i < MapH; i++)
        {
            if (i == 0 || i == MapH - 1)
            { 
                for (int j = 0; j < MapW; j++)
                {
                       MapGrid[i, j] = 0;
                }
            }
            else
            {
                MapGrid[i, 0] = 0;
                MapGrid[i, MapW - 1] = 0;
            }
        }
    }


    void Decimate()
    {
        int count0;
        int count1;
        for (int i = 1; i < MapH - 1; i++)
        {
            for (int j = 1; j < MapW - 1; j++)
            {
                count0 = 0;
                count1 = 0;
                for (var ii = -1; ii <= 1; ii++)
                {
                    if ((MapGrid[i + ii, j] == 0) && (MapGrid[i, j] == 1)) count0++;
                    if ((MapGrid[i + ii, j] == 1) && (MapGrid[i, j] == 0)) count1++;
                }
                for (var jj = -1; jj <= 1; jj++)
                {
                    if ((MapGrid[i, j + jj] == 0) && (MapGrid[i, j] == 1)) count0++;
                    if ((MapGrid[i, j + jj] == 1) && (MapGrid[i, j] == 0)) count1++;
                }

                if (count0 >= 5) MapGrid[i, j] = 0;
                if (count1 >= 4) MapGrid[i, j] = 1;
            }
        }
    }

    void RandWorms()
    {
        int posi;
        int posj;
        int shift;
        for (int n = 1; n < 10; n++)
        {
            posi= Random.Range(0, MapH);
            posj= Random.Range(0, MapW);

            for (int i = 1; i < Mathf.Min(MapW, MapH)-3; i++)
            {
                
                
                MapGrid[posi, posj] = 0;
                shift = Random.Range(-1, 2);
                if (shift == 0)
                {
                    shift = Random.Range(-1, 2);
                    posj += shift;
                    Debug.Log("j"+posj);
                    posj = CheckBounds(posj, MapW - 1);
                }
                else
                {
                    posi += shift;
                    Debug.Log("i"+posi);
                    posi = CheckBounds(posi, MapH - 1);
                }

            }
        }
    }

    int CheckBounds(int i,int max)
    {
        if (i < 0) i = 0;
        if (i > max) i = max;
        return i;
    }


        void CreateEntrance()
    {
        MapGrid[MapH - 1, (int)(MapW / 2)] = 2;
        for (int i = MapH - 3; i < MapH - 1; i++)
        {
            for (int j = (int)(MapW / 2); j < (int)(MapW / 2) + 2; j++)
            {
                MapGrid[i, j] = 1;
            }
        }
    }



    void ConsoleDisplay()
    {
        DebugStr = "";
        for (int i = 0; i < MapH; i++)
        {
            for (int j = 0; j < MapW; j++)
            {
                DebugStr = string.Concat(DebugStr, MapGrid[i, j].ToString());
            }
            DebugStr = string.Concat(DebugStr, "\n");
        }
        Debug.Log(DebugStr);
    }

}
