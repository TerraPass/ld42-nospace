﻿using UnityEngine;

namespace NoSpace.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicPlayer : MonoBehaviour
    {
        [SerializeField]
        private AudioClip[] backgroundMusicClips;

        private AudioSource myAudioSource;

        private bool isStopped = true;

        void Start()
        {
            this.myAudioSource = GetComponent<AudioSource>();
        }

        void Update()
        {
            if(!isStopped && (this.myAudioSource.clip == null || !this.myAudioSource.isPlaying))
            {
                this.myAudioSource.clip = backgroundMusicClips[Random.Range(0, backgroundMusicClips.Length)];
                this.myAudioSource.Play();
            }
        }

        public void Play()
        {
            this.isStopped = false;
        }

        public void Stop()
        {
            this.myAudioSource.Stop();
            this.isStopped = true;
        }
    }
}
