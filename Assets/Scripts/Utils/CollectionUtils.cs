using System.Collections.Generic;
using UnityEngine;

namespace NoSpace.Utils
{
    public static class CollectionUtils
    {
        public static T GetRandomElement<T>(List<T> collection)
        {

            return collection[(int)(UnityEngine.Random.value*collection.Count)];
        }
    }
}