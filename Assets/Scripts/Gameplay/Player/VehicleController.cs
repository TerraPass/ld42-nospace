using UnityEngine;

using NoSpace.Gameplay.Grid;
using NoSpace.Gameplay.Vehicles;

namespace NoSpace.Gameplay.Player
{
    public class VehicleController : MonoBehaviour
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private LevelDirector levelDirector;

        [SerializeField]
        private Camera inputCamera;

        [SerializeField]
        private AbstractGrid inputGrid;

        [SerializeField]
        private float raycastMaxDistance = 100.0f;

        [SerializeField]
        private LayerMask raycastLayerMask;

        [SerializeField]
        private DestinationHighlight destinationHighlight;

        //
        // Fields
        //

        private AbstractVehicle controlledVehicle;
        private AbstractVehicle pendingVehicle;

        //
        // Unity methods
        //

        void Update()
        {
            if (controlledVehicle == null && pendingVehicle == null)
            {
                destinationHighlight.State = DestinationHighlightState.Disabled;
                return;
            }

            Ray ray = inputCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, raycastMaxDistance, raycastLayerMask))
            {
                Vector2Int gridPosition = inputGrid.View.ToGridPosition(hit.point);

                bool hasControlledVehicle = (controlledVehicle != null);
                bool canGoToPosition = hasControlledVehicle && !WillPreventEntry(gridPosition) && controlledVehicle.CanReachDestination(gridPosition);

                destinationHighlight.State = canGoToPosition
                    ? DestinationHighlightState.OK 
                    : (hasControlledVehicle ? DestinationHighlightState.Unreachable : DestinationHighlightState.NotReady);
                destinationHighlight.Dimensions = hasControlledVehicle
                    ? controlledVehicle.CurrentDimensions
                    : pendingVehicle.DefaultDimensions;

                destinationHighlight.GridPosition = gridPosition;

                if (hasControlledVehicle && Input.GetMouseButtonDown(0))
                {
                    if (controlledVehicle.State == VehicleState.Selected && inputGrid.CellStates.Contains(gridPosition))
                    {
                        if (canGoToPosition)
                        {
                            controlledVehicle.StartMovingToDestination(gridPosition);
                            levelDirector.OnVehicleDispatched(controlledVehicle.CurrentDimensions);

                            controlledVehicle = null;
                        }
                    }
                }
            }
            else
            {
                destinationHighlight.State = DestinationHighlightState.Disabled;
            }
        }

        //
        // Properties
        //

        public AbstractVehicle ControlledVehicle
        {
            get {
                return controlledVehicle;
            }
            set {
                controlledVehicle = value;
            }
        }

        public AbstractVehicle PendingVehicle
        {
            get {
                return pendingVehicle;
            }
            set {
                pendingVehicle = value;
            }
        }

        //
        // Detail methods
        //

        // FIXME: This needs to be rewritten without the assumption
        //        that the entrance will always be 2 cells wide.
        private bool WillPreventEntry(Vector2Int cell)
        {
            return cell == inputGrid.EntranceCell || cell == inputGrid.EntranceCell + Vector2Int.right;
        }
    }
}