namespace NoSpace.Gameplay.Player
{
    public enum DestinationHighlightState
    {
        Disabled    = 0,
        NotReady    = 1,
        Unreachable = 2,
        // TODO: Dubious = 3?
        OK          = 3
    }
}