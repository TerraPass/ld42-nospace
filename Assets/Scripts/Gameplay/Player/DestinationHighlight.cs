using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using NoSpace.Gameplay.Grid;
using NoSpace.Gameplay.Grid.View;

namespace NoSpace.Gameplay.Player
{
    public class DestinationHighlight : MonoBehaviour
    {
        //
        // Inspector types
        //

        [Serializable]
        private struct HighlightCellPrefabMapping
        {
            public DestinationHighlightState state;
            public GameObject cellPrefab;
        }

        //
        // Inspector fields
        //

        [SerializeField]
        private GridView gridView;

        [SerializeField]
        private HighlightCellPrefabMapping[] highlightCellPrefabMappings;

        //
        // Fields
        //

        private IDictionary<DestinationHighlightState, GameObject> highlightCellPrefabs;
        private List<GameObject> activeHighlightCells;

        private Vector2Int gridPosition;
        private Vector2Int dimensions;
        private DestinationHighlightState state;
        private bool dirty;

        //
        // Properties
        //

        public Vector2Int GridPosition
        {
            get {
                return this.gridPosition;
            }
            set {
                this.gridPosition = value;
            }
        }

        public Vector2Int Dimensions
        {
            get {
                return this.dimensions;
            }
            set {
                this.dimensions = value;
                this.dirty = true;
            }
        }

        public DestinationHighlightState State
        {
            get {
                return this.state;
            }
            set {
                this.state = value;
                this.dirty = true;
            }
        }

        //
        // Unity methods
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            this.highlightCellPrefabs = new Dictionary<DestinationHighlightState, GameObject>();
            foreach (var mapping in this.highlightCellPrefabMappings)
            {
                DebugUtils.Assert(
                    !this.highlightCellPrefabs.ContainsKey(mapping.state),
                    "Duplicate cell prefabs specified for state {0}",
                    mapping.state
                );

                this.highlightCellPrefabs.Add(mapping.state, mapping.cellPrefab);
            }

            this.activeHighlightCells = new List<GameObject>();

            this.dimensions = new Vector2Int(1, 1);
            this.state      = DestinationHighlightState.Disabled;

            this.dirty = true;
        }

        void Update()
        {
            this.transform.position = this.gridView.ToVisualPosition(this.gridPosition);

            if(!this.dirty)
                return;

            // Remove current cells
            foreach (var cellObject in activeHighlightCells)
            {
                Destroy(cellObject);
            }
            activeHighlightCells.Clear();

            // If the current state has no prefab specified, we don't need any cells
            if (!highlightCellPrefabs.ContainsKey(state))
            {
                DebugUtils.Assert(
                    state == DestinationHighlightState.Disabled,
                    "Highlight prefab must be specified for state {0}",
                    state
                );
                return;
            }

            // Instantiate up-to-date cells
            activeHighlightCells.Capacity = dimensions.x * dimensions.y;
            for (int y = 0; y < dimensions.y; y++)
            {
                for (int x = 0; x < dimensions.x; x++)
                {
                    var gridPositionDelta = new Vector2Int(x, y);
                    var cellObject = Instantiate(
                        this.highlightCellPrefabs[state],
                        this.transform.position
                            + this.gridView.GetVisualDelta(gridPositionDelta, Vector2Int.zero),
                        Quaternion.identity,
                        this.transform
                    );
                    activeHighlightCells.Add(cellObject);
                }
            }
        }
    }
}