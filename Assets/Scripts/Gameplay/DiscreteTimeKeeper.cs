using System;
using UnityEngine;

using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace NoSpace.Gameplay
{
    public class DiscreteTimeKeeper : AbstractDiscreteTimeKeeper
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private float fixedTimePerTick = 1.0f;

        //
        // Fields
        //

        private int discreteTime;
        private float tickProgress;

        private float lastTickFixedTime;

        //
        // Unity method
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            this.discreteTime = 0;
            this.lastTickFixedTime = Time.fixedTime;
            this.tickProgress = 0.0f;
        }

        void FixedUpdate()
        {
            if (Time.fixedTime > lastTickFixedTime + fixedTimePerTick)
            {
                discreteTime++;
                lastTickFixedTime = Time.fixedTime;

                if (this.DiscreteTick != null)
                {
                    this.DiscreteTick(this, new DiscreteTickEventArgs(this));
                }
            }

            tickProgress = (Time.fixedTime - lastTickFixedTime) / fixedTimePerTick;

            DebugUtils.Assert(tickProgress >= 0.0f && tickProgress <= 1.0f, "Tick progress must be within range [0.0f, 1.0f]");
        }

        //
        // Events
        //

        public override event EventHandler<DiscreteTickEventArgs> DiscreteTick;

        //
        // Properties
        //

        public override int DiscreteTime
        {
            get {
                return discreteTime;
            }
        }

        public override float TickProgress
        {
            get {
                return tickProgress;
            }
        }
    }
}