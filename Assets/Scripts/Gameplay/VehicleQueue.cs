using System;
using System.Collections.Generic;
using UnityEngine;

using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using NoSpace.Gameplay.Vehicles;
using NoSpace.Gameplay.Grid;

namespace NoSpace.Gameplay
{
    public class VehicleQueue : MonoBehaviour
    {
        //
        // Constants
        //

        private const GridCellState DummyVehicleMarker = GridCellState.Obstacle;

        //
        // Inspector fields
        //

        // [SerializeField]
        // private GameplayServiceLocator gameplayServices;

        [SerializeField]
        private AbstractGrid queueGrid;

        [SerializeField]
        private Vector2Int frontCell;

        //
        // Fields
        //

        private Queue<AbstractVehicle> queue;

        private bool frontAtDestination;

        private Vector2Int dummyVehicleDimensions;

        //
        // Unity methods
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            this.queue              = new Queue<AbstractVehicle>();
            this.frontAtDestination = false;

            this.dummyVehicleDimensions = Vector2Int.zero;
        }

        //
        // Methods
        //

        public void ReleaseAllVehicles()
        {
            this.queue.Clear();
            this.frontAtDestination = false;
        }

        public bool CanPushVehicle(Vector2Int vehicleDimensions)
        {
            return queueGrid.CanSpawnVehicle(vehicleDimensions);
        }

        public void PushVehicle(AbstractVehicle vehicle)
        {
            DebugUtils.Assert(queueGrid.CanSpawnVehicle(vehicle.CurrentDimensions), "Grid must be ready to spawn vehicle");
            vehicle.SpawnOnGrid(queueGrid);

            this.queue.Enqueue(vehicle);

            vehicle.ReachedDestination += this.OnVehicleReachedDestination;

            // TODO: Ensure that we don't need a more liberal route planner while in queue
            vehicle.StartMovingToDestination(frontCell);
        }

        public bool CanPopVehicle()
        {
            return this.queue.Count > 0 && frontAtDestination;
        }

        public AbstractVehicle PopVehicle()
        {
            DebugUtils.Assert(this.queue.Count > 0, "queue must not be empty");
            this.frontAtDestination = false;
            return this.queue.Dequeue();
        }

        public void InstallDummyVehicle(Vector2Int dimensions)
        {
            DebugUtils.Assert(dummyVehicleDimensions == Vector2Int.zero, "Dummy vehicle already installed");
            DebugUtils.Assert(dimensions != Vector2Int.zero, "Must install non-zero sized dummy vehicle");

            for (int y = 0; y < dimensions.y; y++)
            {
                for (int x = 0; x < dimensions.x; x++)
                {
                    Vector2Int gridPosition = this.frontCell + new Vector2Int(x, y);

                    DebugUtils.Assert(
                        queueGrid.CellStates[gridPosition] == GridCellState.Empty,
                        "Every cell under the installed dummy vehicle must be empty"
                    );

                    queueGrid.CellStates[gridPosition] = DummyVehicleMarker;
                }
            }

            this.dummyVehicleDimensions = dimensions;
        }

        public bool RemoveDummyVehicle()
        {
            if (dummyVehicleDimensions == Vector2Int.zero)
                return false;

            for (int y = 0; y < dummyVehicleDimensions.y; y++)
            {
                for (int x = 0; x < dummyVehicleDimensions.x; x++)
                {
                    Vector2Int gridPosition = this.frontCell + new Vector2Int(x, y);

                    DebugUtils.Assert(
                        queueGrid.CellStates[gridPosition] == DummyVehicleMarker,
                        "Every cell under the dummy vehicle must have marker state {0}",
                        DummyVehicleMarker
                    );

                    queueGrid.CellStates[gridPosition] = GridCellState.Empty;
                }
            }

            this.dummyVehicleDimensions = Vector2Int.zero;

            return true;
        }

        //
        // Properties
        //

        public AbstractVehicle Front
        {
            get {
                return this.queue.Peek();
            }
        }

        //
        // Detail methods
        //

        private void OnVehicleReachedDestination(object sender, VehicleReachedDestinationEventArgs args)
        {
            DebugUtils.Assert(!this.frontAtDestination, "frontAtDestination must be false at this point");
            DebugUtils.Assert(args.Vehicle == this.Front, "Front vehicle is expected to be the first to reach destination");

            this.frontAtDestination = true;
            args.Vehicle.ReachedDestination -= this.OnVehicleReachedDestination;
        }
    }
}