using UnityEngine;

using Terrapass.Extensions.Unity;

namespace NoSpace.Gameplay.Grid.View
{
    public class GridView : MonoBehaviour
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private Vector3 gridOriginOffset = Vector3.zero;

        [SerializeField]
        private float gridStep = 1.0f;

        //
        // Fields
        //

        private Vector3 gridOrigin;
        private Vector3 cellCenterDelta;

        //
        // Unity methods
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            this.gridOrigin = transform.position + gridOriginOffset;
            this.cellCenterDelta = new Vector3(0.5f*gridStep, 0.0f, -0.5f*gridStep);
        }

        //
        // Methods
        //

        public Vector3 ToVisualPosition(Vector2Int gridPosition)
        {
            return gridOrigin
                + gridStep*gridPosition.x*Vector3.right
                + gridStep*gridPosition.y*Vector3.back;
        }

        public Vector3 ToVisualPositionCentered(Vector2Int gridPosition)
        {
            return ToVisualPosition(gridPosition) + cellCenterDelta;
        }

        public Vector3 GetVisualDelta(Vector2Int gridPosition0, Vector2Int gridPosition1)
        {
            return ToVisualPosition(gridPosition0) - ToVisualPosition(gridPosition1);
        }

        public Vector2Int ToGridPosition(Vector3 visualPosition)
        {
            return new Vector2Int(
                (int)((visualPosition.x - gridOrigin.x) / gridStep),
                (int)((gridOrigin.z - visualPosition.z) / gridStep)
            );
        }

        //
        // Properties
        //

        // TODO: Replace with a Vector2Int and use it instead of hardcoded
        //       Vector2Int.down and Vector2Int.right values.
        public float GridStep
        {
            get {
                return gridStep;
            }
        }
    }
}