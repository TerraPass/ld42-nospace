using UnityEngine;

using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using NoSpace.Gameplay.Vehicles;
using NoSpace.Gameplay.Grid.Layout;
using NoSpace.Gameplay.Grid.Routing;
using NoSpace.Gameplay.Grid.View;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NoSpace.Gameplay.Grid
{
    public class ParkingGrid : AbstractGrid
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private AbstractGridLayoutProvider gridLayoutProvider;

        [SerializeField]
        private GridView view;

        //
        // Fields
        //

        private Vector2Int entranceCell;
        private GridData<GridCellState> cellStates;

        private IRoutePlanner routePlanner;

        //
        // Unity methods
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            LoadLayout(gridLayoutProvider);
        }

        //
        // Interface
        //

        public override void ResetCellStates()
        {
            for (int y = 0; y < cellStates.Height; y++)
            {
                for (int x = 0; x < cellStates.Width; x++)
                {
                    cellStates[x, y] = cellStates[x, y] == GridCellState.Obstacle
                        ? GridCellState.Obstacle
                        : GridCellState.Empty;
                }
            }
        }

        public override void Initialize(IRoutePlanner routePlanner)
        {
            this.routePlanner = routePlanner;
        }

        public override bool CanSpawnVehicle(Vector2Int vehicleDimensions)
        {
            for (int dy = 0; dy < vehicleDimensions.y; dy++)
            {
                for (int dx = 0; dx < vehicleDimensions.x; dx++)
                {
                    Vector2Int gridPosition = entranceCell + new Vector2Int(dx, dy);

                    // Ignore positions outside of grid
                    if (!cellStates.Contains(gridPosition))
                        continue;

                    if (!IsCellReadyForMove(gridPosition))
                        return false;
                }
            }

            return true;
        }

        public override bool CanStartMoving(AbstractVehicle vehicle, Vector2Int toCell)
        {
            for (int dy = 0; dy < vehicle.CurrentDimensions.y; dy++)
            {
                for (int dx = 0; dx < vehicle.CurrentDimensions.x; dx++)
                {
                    Vector2Int gridPosition = toCell + new Vector2Int(dx, dy);

                    // Ignore positions outside of grid
                    if (!cellStates.Contains(gridPosition))
                        continue;

                    // Ignore positions, currently belonging to this same vehicle
                    if (vehicle.State != VehicleState.Invalid   // Skip this check for spawning vehicles
                        && gridPosition.x >= vehicle.GridPosition.x
                        && gridPosition.x <  vehicle.GridPosition.x + vehicle.CurrentDimensions.x
                        && gridPosition.y >= vehicle.GridPosition.y
                        && gridPosition.y <  vehicle.GridPosition.y + vehicle.CurrentDimensions.y)
                    {
                        continue;
                    }

                    if (!IsCellReadyForMove(gridPosition))
                        return false;
                }
            }

            return true;
        }

        public override void OnVehicleSpawned(AbstractVehicle vehicle)
        {
            DebugUtils.Assert(cellStates[vehicle.GridPosition] == GridCellState.Empty, "Vehicle must spawn onto an empty cell");

            ApplyCellsState(vehicle, vehicle.GridPosition, GridCellState.OccupiedTransit);
        }

        public override void OnVehicleDespawned(AbstractVehicle vehicle)
        {
            ApplyCellsState(vehicle, vehicle.GridPosition, GridCellState.Empty);
        }

        public override void OnVehicleStartedMoving(AbstractVehicle vehicle, Vector2Int fromCell, Vector2Int toCell)
        {
            DebugUtils.Assert(cellStates[fromCell] == GridCellState.OccupiedTransit, "Can start moving only from an OccupiedTransit cell");
            //DebugUtils.Assert(cellStates[toCell] == GridCellState.Empty, "Can start moving only to an Empty cell");

            ApplyCellsState(vehicle, fromCell, GridCellState.Empty);    // TODO: Reconsider?
            ApplyCellsState(vehicle, toCell,   GridCellState.Locked);
        }

        public override void OnVehicleMoved(AbstractVehicle vehicle, Vector2Int fromCell, Vector2Int toCell)
        {
            DebugUtils.Assert(vehicle.State == VehicleState.InTransitMoving, "Vehicle must be InTransitMoving at this point");
            //DebugUtils.Assert(cellStates[fromCell] == GridCellState.OccupiedTransit, "Must move from OccupiedTransit cell");

            // FIXME: Inefficient! Overdrawing + iterating twice.
            //ApplyCellsState(vehicle, fromCell, GridCellState.Empty);
            ApplyCellsState(vehicle, toCell, GridCellState.OccupiedTransit);
        }

        public override void OnVehicleReachedDestination(AbstractVehicle vehicle)
        {
            DebugUtils.Assert(vehicle.State == VehicleState.ReachedDestination, "Vehicle must be in ReachedDestination state");

            ApplyCellsState(vehicle, vehicle.GridPosition, GridCellState.OccupiedStationary);
        }

        //
        // Detail methods
        //

        private void LoadLayout(AbstractGridLayoutProvider gridLayoutProvider)
        {
            var rawGridLayout = gridLayoutProvider.GetGridLayout();
            this.entranceCell = rawGridLayout.EntranceRect.position;

            var gridLayout = rawGridLayout.Data;
            this.cellStates = new GridData<GridCellState>(gridLayout.Width, gridLayout.Height);
            for (int y = 0; y < cellStates.Height; y++)
            {
                for (int x = 0; x < cellStates.Width; x++)
                {
                    this.cellStates[x, y] = gridLayout[x, y] == GridLayoutCell.Obstacle
                                                ? GridCellState.Obstacle
                                                : GridCellState.Empty;
                }
            }
        }

        private bool IsCellStateReadyForMove(GridCellState state)
        {
            return state == GridCellState.Empty;
        }

        private bool IsCellReadyForMove(Vector2Int cell)
        {
            return IsCellStateReadyForMove(cellStates[cell]);
        }

        private void ApplyCellsState(AbstractVehicle vehicle, Vector2Int topLeft, GridCellState state)
        {
            for (int dx = 0; dx < vehicle.CurrentDimensions.x; dx++)
            {
                for (int dy = 0; dy < vehicle.CurrentDimensions.y; dy++)
                {
                    Vector2Int delta = new Vector2Int(dx, dy);
                    Vector2Int gridPosition = topLeft + delta;

                    // Ignore positions outside of grid
                    if (!cellStates.Contains(gridPosition))
                        continue;

                    cellStates[topLeft + delta] = state;
                }
            }
        }

        //
        // Properties
        //

        public override Vector2Int Dimensions
        {
            get {
                return this.cellStates.Dimensions;
            }
        }

        public override Vector2Int EntranceCell
        {
            get {
                return this.entranceCell;
            }
        }

        public override GridData<GridCellState> CellStates
        {
            get {
                return this.cellStates;
            }
        }

        public override GridView View
        {
            get {
                return this.view;
            }
        }

        public override IRoutePlanner RoutePlanner
        {
            get {
                return this.routePlanner;
            }
        }

        //
        // Debug methods
        //

        void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying)
                return;

            #if UNITY_EDITOR

            for (int y = 0; y < cellStates.Height; y++)
            {
                for (int x = 0; x < cellStates.Width; x++)
                {
                    Vector2Int gridPosition = new Vector2Int(x, y);

                    Vector3 visualPosition = view.ToVisualPosition(gridPosition);
                    Vector3 visualPositionCentered = view.ToVisualPositionCentered(gridPosition);

                    Handles.color = GetCellStateColor(cellStates[gridPosition]);
                    Handles.Label(visualPosition, cellStates[gridPosition].ToString());
                    Handles.RectangleHandleCap(0, visualPositionCentered, Quaternion.Euler(-90.0f, 0.0f, 0.0f), view.GridStep/2.0f, EventType.Repaint);

                    if (gridPosition == entranceCell)
                    {
                        Gizmos.DrawCube(visualPositionCentered, new Vector3(0.25f, 3.0f, 0.25f));
                    }
                }
            }

            #endif
        }

        #if UNITY_EDITOR

        private static Color GetCellStateColor(GridCellState cellState)
        {
            switch (cellState)
            {
                default                               : return Color.magenta;
                case GridCellState.Empty              : return Color.white;
                case GridCellState.Locked             : return Color.red;
                case GridCellState.OccupiedTransit    : return Color.green;
                case GridCellState.OccupiedStationary : return Color.blue;
                case GridCellState.Obstacle           : return Color.black;
            }
        }

        #endif
    }
}