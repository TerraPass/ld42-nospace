using UnityEngine;

using NoSpace.Gameplay.Vehicles;
using NoSpace.Gameplay.Grid.Routing;
using NoSpace.Gameplay.Grid.View;

namespace NoSpace.Gameplay.Grid
{
    public abstract class AbstractGrid : MonoBehaviour
    {
        //
        // Properties
        //

        public abstract Vector2Int Dimensions {get;}
        public abstract Vector2Int EntranceCell {get;}
        public abstract GridData<GridCellState> CellStates {get;}

        public abstract GridView View {get;}

        // TODO: This should probably not be here
        public abstract IRoutePlanner RoutePlanner {get;}

        //
        // Methods
        //

        public abstract void ResetCellStates();

        public abstract void Initialize(IRoutePlanner routePlanner);

        public abstract bool CanSpawnVehicle(Vector2Int vehicleDimensions);
        public abstract bool CanStartMoving(AbstractVehicle vehicle, Vector2Int toCell);

        public abstract void OnVehicleSpawned(AbstractVehicle vehicle);
        public abstract void OnVehicleDespawned(AbstractVehicle vehicle);

        public abstract void OnVehicleStartedMoving(AbstractVehicle vehicle, Vector2Int fromCell, Vector2Int toCell);
        public abstract void OnVehicleMoved(AbstractVehicle vehicle, Vector2Int fromCell, Vector2Int toCell);
        public abstract void OnVehicleReachedDestination(AbstractVehicle vehicle);
    }
}