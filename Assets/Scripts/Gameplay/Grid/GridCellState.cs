namespace NoSpace.Gameplay.Grid
{
    public enum GridCellState
    {
        Empty              = 0,
        Locked             = 2,
        OccupiedTransit    = 3,
        OccupiedStationary = 4,
        Obstacle           = 5
    }
}