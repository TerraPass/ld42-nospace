using System;
using UnityEngine;

using Terrapass.Debug;

using GridLayout = NoSpace.Gameplay.Grid.Layout.RawGridLayout;

namespace NoSpace.Gameplay.Grid.Layout
{
    public class LShapedGridLayoutProvider : AbstractGridLayoutProvider
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private Vector2Int dimensions;
        [SerializeField]
        private Vector2Int cutoutPosition;

        [SerializeField]
        private RectInt entranceRect;

        //
        // Fields
        //

        private RawGridLayout? layout;

        //
        // IGridLayoutProvider
        //

        public override RawGridLayout GetGridLayout()
        {
            if (this.layout == null)
            {
                var data = new GridData<GridLayoutCell>(dimensions.x, dimensions.y, GridLayoutCell.Empty);

                for (int y = 0; y <= cutoutPosition.y; y++)
                {
                    for (int x = data.Width - 1; x >= cutoutPosition.x; x--)
                    {
                        data[x, y] = GridLayoutCell.Obstacle;
                    }
                }

                this.layout = new RawGridLayout(data, entranceRect);
            }

            return this.layout.Value;
        }
    }
}