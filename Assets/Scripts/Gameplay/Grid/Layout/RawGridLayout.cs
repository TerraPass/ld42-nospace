using UnityEngine;

namespace NoSpace.Gameplay.Grid.Layout
{
    public struct RawGridLayout
    {
        public GridData<GridLayoutCell> Data         {get;}
        public RectInt                  EntranceRect {get;}

        public RawGridLayout(
            GridData<GridLayoutCell> data,
            RectInt                  entranceRect
        )
        {
            this.Data         = data;
            this.EntranceRect = entranceRect;
        }
    }
}