namespace NoSpace.Gameplay.Grid.Layout
{
    public interface IGridLayoutProvider
    {
        RawGridLayout GetGridLayout();
    }
}