using UnityEngine;

namespace NoSpace.Gameplay.Grid.Layout
{
    public abstract class AbstractGridLayoutProvider : MonoBehaviour, IGridLayoutProvider
    {
        public abstract RawGridLayout GetGridLayout();
    }
}