namespace NoSpace.Gameplay.Grid.Layout
{
    public enum GridLayoutCell
    {
        Empty    = 0,
        Obstacle = 1
    }
}