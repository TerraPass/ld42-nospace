using UnityEngine;

using Terrapass.Debug;

namespace NoSpace.Gameplay.Grid.Layout
{
    public class HardcodedGridLayoutProvider : AbstractGridLayoutProvider
    {
        //
        // Constants
        //

        //private const int EmptyCell    = 0;
        private const int BlockedCell  = 1;

        // Config

        private const int Width = 15;

        private static readonly int[] RawGridLayout = new int[]{
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1,
            1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 2, 0, 1, 1, 1, 1, 1, 1
        };

        private static readonly RectInt EntranceRect = new RectInt(
            new Vector2Int(7, 8),
            new Vector2Int(2, 1)
        );

        //
        // Fields
        //

        private GridData<GridLayoutCell> data = null;

        //
        // IGridLayoutProvider
        //

        public override RawGridLayout GetGridLayout()
        {
            if (data == null)
            {
                ReadConfig();
            }

            return new RawGridLayout(data, EntranceRect);
        }

        //
        // Detail methods
        //

        private void ReadConfig()
        {
            var rawGridLayout = RawGridLayout;

            this.data = new GridData<GridLayoutCell>(
                Width,
                rawGridLayout.Length / Width
            );

            for (int y = 0; y < data.Height; y++)
            {
                for (int x = 0; x < data.Width; x++)
                {
                    int rawCellValue = rawGridLayout[y*data.Width + x];
                    data[x, y] = rawCellValue == BlockedCell
                                    ? GridLayoutCell.Obstacle 
                                    : GridLayoutCell.Empty;
                }
            }
        }
    }
}