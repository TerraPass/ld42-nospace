using System.Collections.Generic;
using UnityEngine;

using Terrapass.Debug;

namespace NoSpace.Gameplay.Grid.Routing
{
    public struct Route
    {
        public readonly LinkedList<Vector2Int> waypoints;

        public Route(LinkedList<Vector2Int> waypoints)
        {
            this.waypoints = waypoints;
        }

        public bool IsValid
        {
            get {
                return waypoints != null;
            }
        }
    }
}