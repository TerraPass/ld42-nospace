using UnityEngine;

using NoSpace.Gameplay.Grid;

namespace NoSpace.Gameplay.Grid.Routing
{
    // TODO: Can be easily made generic.
    public class GridPreprocessor
    {
        private readonly int blockedCellValue;

        public GridPreprocessor(int blockedCellValue)
        {
            this.blockedCellValue = blockedCellValue;
        }

        public void PreprocessGrid(
            GridData<int> grid,
            int objectSize
        )
        {
            for (int y = 0; y < grid.Height; y++) // sic! skip the first several rows
            {
                for (int x = 0; x < grid.Width; x++) // sic! skip the first several columns
                {
                    if (grid[x, y] != blockedCellValue)
                        continue;

                    for (int delta = 1; delta < objectSize; delta++)
                    {
                        if (grid.Contains(x - delta, y))
                            grid[x - delta, y] = blockedCellValue;
                        if (grid.Contains(x, y - delta))
                            grid[x, y - delta] = blockedCellValue;
                    }
                }
            }
        }
    }
}