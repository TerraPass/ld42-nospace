using System;
using UnityEngine;

using NoSpace.Gameplay.Grid;

namespace NoSpace.Gameplay.Grid.Routing
{
    public interface IRoutePlanner//<T>
        //where T : struct, IComparable, IConvertible
    {
        Route PlanRoute(
            GridData<GridCellState> grid,
            Vector2Int  sourceTopLeft,
            Vector2Int  destinationTopLeft,
            Vector2Int  objectDimensions
        );
    }
}