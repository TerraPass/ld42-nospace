using System.Collections.Generic;
using UnityEngine;

using Terrapass.Debug;

using NoSpace.Gameplay.Grid;

namespace NoSpace.Gameplay.Grid.Routing
{
    public class CompositeRoutePlanner : IRoutePlanner
    {
        //
        // Fields
        //

        private List<IRoutePlanner> planners;

        //
        // Constructors
        //

        public CompositeRoutePlanner()
        {
            this.planners = new List<IRoutePlanner>();
        }

        //
        // Methods
        //

        public Route PlanRoute(
            GridData<GridCellState> grid,
            Vector2Int sourceTopLeft,
            Vector2Int destinationTopLeft,
            Vector2Int objectDimensions
        )
        {
            foreach (var planner in planners)
            {
                var route = planner.PlanRoute(
                    grid,
                    sourceTopLeft,
                    destinationTopLeft,
                    objectDimensions
                );

                if (route.IsValid)
                    return route;
            }

            return new Route(null);
        }

        public void AddPlanner(IRoutePlanner planner)
        {
            planners.Add(PreconditionUtils.EnsureNotNull(planner, "planner"));
        }
    }
}