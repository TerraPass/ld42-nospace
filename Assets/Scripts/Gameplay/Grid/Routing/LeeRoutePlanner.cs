//#define NOSPACE_DEBUG_WAVEGRID

using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using UnityEngine;

using Terrapass.Debug;
using Terrapass.Collections;

using NoSpace.Gameplay.Grid;

namespace NoSpace.Gameplay.Grid.Routing
{
    public class LeeRoutePlanner/*<T>*/ : IRoutePlanner//<T>
        //where T : struct, IComparable, IConvertible
    {
        //
        // Constants
        //

        private const int EmptyCellValue   = -2;
        private const int BlockedCellValue = -1;

        //
        // Fields
        //

        private readonly GridPreprocessor gridPreprocessor;

        private readonly HashSet<GridCellState> blockedStates;

        private GridData<int> waveGrid = null;

        //
        // Constructor
        //

        public LeeRoutePlanner(params GridCellState[] blockedStates)
        {
            this.gridPreprocessor = new GridPreprocessor(BlockedCellValue);
            this.blockedStates = new HashSet<GridCellState>(blockedStates.AsEnumerable());
        }

        //
        // IRoutePlanner
        //

        public Route PlanRoute(
            GridData<GridCellState> grid,
            Vector2Int  source,
            Vector2Int  destination,
            Vector2Int  objectDimensions
        )
        {
            DebugUtils.Assert(objectDimensions.x == objectDimensions.y, "Only squares are currently supported :(");
            int objectSize = objectDimensions.x;

            if (waveGrid == null)
            {
                waveGrid = new GridData<int>(grid.Width, grid.Height);
            }

            DebugUtils.Assert(grid.Width == waveGrid.Width && grid.Height == waveGrid.Height, "Grid dimensions must never change");

            // Reset waveGrid according to the current grid state
            for (int y = 0; y < grid.Height; y++)
            {
                for (int x = 0; x < grid.Width; x++)
                {
                    // Force empty cells on the object's footprint
                    Vector2Int currentCell = new Vector2Int(x, y);
                    Vector2Int sourceDelta = currentCell - source;
                    if (sourceDelta.x >= 0 && sourceDelta.x < objectDimensions.x
                        && sourceDelta.y >= 0 && sourceDelta.y < objectDimensions.y)
                    {
                        waveGrid[x, y] = EmptyCellValue;
                        continue;
                    }

                    waveGrid[x, y] = IsBlockedState(grid[x, y]) ? BlockedCellValue : EmptyCellValue;
                }
            }

            LinkedList<Vector2Int> waveFront     = new LinkedList<Vector2Int>();

            if (objectSize > 1)
            {
                gridPreprocessor.PreprocessGrid(waveGrid, objectSize);
            }

            //if (waveGrid[source] == BlockedCellValue)
                //throw new ArgumentException("Object overlaps an obstacle at route source");

            // Init source cell
            waveGrid[source] = 0;

            waveFront.AddLast(source);

            // Spread the wave
            while (waveFront.Count > 0)
            {
                Vector2Int current = waveFront.First();
                waveFront.RemoveFirst();

                if (current == destination)
                    break;

                if (current.x > 0 && waveGrid[current + Vector2Int.left] == EmptyCellValue)
                {
                    waveGrid[current + Vector2Int.left] = waveGrid[current] + 1;
                    waveFront.AddLast(current + Vector2Int.left);
                }
                if (current.y > 0 && waveGrid[current + Vector2Int.down] == EmptyCellValue)
                {
                    waveGrid[current + Vector2Int.down] = waveGrid[current] + 1;
                    waveFront.AddLast(current + Vector2Int.down);
                }
                if (current.x < waveGrid.Width - 1 && waveGrid[current + Vector2Int.right] == EmptyCellValue)
                {
                    waveGrid[current + Vector2Int.right] = waveGrid[current] + 1;
                    waveFront.AddLast(current + Vector2Int.right);
                }
                if (current.y < waveGrid.Height - 1 && waveGrid[current + Vector2Int.up] == EmptyCellValue)
                {
                    waveGrid[current + Vector2Int.up] = waveGrid[current] + 1;
                    waveFront.AddLast(current + Vector2Int.up);
                }
            }

            // Will only get called if NOSPACE_DEBUG_WAVEGRID is defined
            DumpGrid(waveGrid);

            // If destination value hasn't been updated it means we can't reach it
            if (waveGrid[destination] < 0)
                return new Route(null);

            // Reconstruct path

            LinkedList<Vector2Int> waypoints = new LinkedList<Vector2Int>();
            Vector2Int currentWaypoint = destination;
            while (waveGrid[currentWaypoint] != 0)
            {
                waypoints.AddFirst(currentWaypoint);

                if (currentWaypoint.x > 0
                    && waveGrid[currentWaypoint + Vector2Int.left] == waveGrid[currentWaypoint] - 1)
                {
                    currentWaypoint = currentWaypoint + Vector2Int.left;
                    continue;
                }

                if (currentWaypoint.y > 0
                    && waveGrid[currentWaypoint + Vector2Int.down] == waveGrid[currentWaypoint] - 1)
                {
                    currentWaypoint = currentWaypoint + Vector2Int.down;
                    continue;
                }

                if (currentWaypoint.x < waveGrid.Width - 1
                    && waveGrid[currentWaypoint + Vector2Int.right] == waveGrid[currentWaypoint] - 1)
                {
                    currentWaypoint = currentWaypoint + Vector2Int.right;
                    continue;
                }

                if (currentWaypoint.y < waveGrid.Height - 1
                    && waveGrid[currentWaypoint + Vector2Int.up] == waveGrid[currentWaypoint] - 1)
                {
                    currentWaypoint = currentWaypoint + Vector2Int.up;
                    continue;
                }

                DebugUtils.Assert(false, "One of the neighbours must have a value less than the current by 1");
                throw new Exception();
            }

            return new Route(waypoints);
        }

        //
        // Detail methods
        //

        private bool IsBlockedState(GridCellState state)
        {
            return blockedStates.Contains(state);
        }

        //
        // Debug methods
        //

        [Conditional("NOSPACE_DEBUG_WAVEGRID")]
        private static void DumpGrid(GridData<int> grid)
        {
            string dump = "";
            for (int y = 0; y < grid.Height; y++)
            {
                for (int x = 0; x < grid.Width; x++)
                {
                    dump += string.Format("\t{0}", grid[x, y]);
                }
                dump += "\n";
            }
            throw new Exception(dump);
        }
    }
}