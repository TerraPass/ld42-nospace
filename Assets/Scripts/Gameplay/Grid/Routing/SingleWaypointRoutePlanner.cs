using System;
using System.Collections.Generic;
using UnityEngine;

using NoSpace.Gameplay.Grid;

namespace NoSpace.Gameplay.Grid.Routing
{
    public class SingleWaypointRoutePlanner : IRoutePlanner
    {
        public Route PlanRoute(
            GridData<GridCellState> grid,
            Vector2Int sourceTopLeft,
            Vector2Int destinationTopLeft,
            Vector2Int objectDimensions
        )
        {
            LinkedList<Vector2Int> routeWaypoints = new LinkedList<Vector2Int>();

            for (int x = sourceTopLeft.x - 1; x >= destinationTopLeft.x; x--)
            {
                routeWaypoints.AddLast(new Vector2Int(x, sourceTopLeft.y));
            }

            for (int y = sourceTopLeft.y - 1; y >= destinationTopLeft.y; y--)
            {
                routeWaypoints.AddLast(new Vector2Int(destinationTopLeft.x, y));
            }

            return new Route(routeWaypoints);
        }
    }
}