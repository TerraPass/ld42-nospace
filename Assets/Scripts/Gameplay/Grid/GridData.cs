using System;
using UnityEngine;

using Terrapass.Debug;

namespace NoSpace.Gameplay.Grid
{
    public sealed class GridData<T>
        where T : struct
    {
        //
        // Fields
        //

        private readonly int width;
        private readonly int height;

        private T[] data;

        //
        // Constructors
        //

        public GridData(int width, int height, T defaultValue)
        {
            this.width = width;
            this.height = height;

            data = new T[height*width];

            Populate(defaultValue);
        }

        public GridData(int width, int height)
            : this(width, height, default(T))
        {
            // FIXME: Suboptimal! We don't need to Populate(); values will be default(T) by default
        }

        //
        // Subscript operators
        //

        public T this [int x, int y]
        {
            get {
                ValidateCoordinates("subscript get", x, y);

                return data[y*width + x];
            }

            set {
                ValidateCoordinates("subscript set", x, y);

                data[y*width + x] = value;
            }
        }

        public T this [Vector2Int position]
        {
            get {
                return this[position.x, position.y];
            }
            set {
                this[position.x, position.y] = value;
            }
        }

        //
        // Methods
        //

        public bool Contains(int x, int y)
        {
            return x >= 0 && x < width && y >= 0 && y < height;
        }

        public bool Contains(Vector2Int position)
        {
            return Contains(position.x, position.y);
        }

        //
        // Detail methods
        //

        private void Populate(T value)
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = value;
            }
        }

        private void ValidateCoordinates(string methodName, int x, int y)
        {
            if (!Contains(x, y))
            {
                throw new ArgumentException(
                    string.Format(
                        "{0}'s {1} invoked with invalid arguments ({2},{3}) for grid size ({4},{5})",
                        this.GetType(),
                        methodName,
                        x, y,
                        width, height
                    )
                );
            }
        }

        //
        // Properties
        //

        public int Width
        {
            get {
                return width;
            }
        }

        public int Height
        {
            get {
                return height;
            }
        }

        public Vector2Int Dimensions
        {
            get {
                return new Vector2Int(width, height);
            }
        }
    }
}