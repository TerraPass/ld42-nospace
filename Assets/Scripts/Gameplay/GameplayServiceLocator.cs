using UnityEngine;

using Terrapass.Extensions.Unity;

using NoSpace.Gameplay.Grid;
using NoSpace.Gameplay.Grid.Routing;
using NoSpace.Gameplay.Vehicles;

namespace NoSpace.Gameplay
{
    public class GameplayServiceLocator : MonoBehaviour
    {
        //
        // Inspector fields
        //

        // [SerializeField]
        // private AbstractGridLayoutProvider primaryGridLayoutProvider;
        // [SerializeField]
        // private AbstractGridLayoutProvider queueGridLayoutProvider;

        [SerializeField]
        private AbstractDiscreteTimeKeeper discreteTimeKeeper;

        [SerializeField]
        private VehicleFactory vehicleFactory;

        //
        // Fields
        //

        private IRoutePlanner primaryRoutePlanner;
        private IRoutePlanner queueRoutePlanner;

        //
        // Unity methods
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            var compositeRoutePlanner = new CompositeRoutePlanner();
            // FIXME: The following reasonably-looking planner never finds a path for 2x2 cars due to self-intersection
            compositeRoutePlanner.AddPlanner(new LeeRoutePlanner(GridCellState.Obstacle, GridCellState.OccupiedStationary, GridCellState.OccupiedTransit));
            compositeRoutePlanner.AddPlanner(new LeeRoutePlanner(GridCellState.Obstacle, GridCellState.OccupiedStationary));
            //compositeRoutePlanner.AddPlanner(new LeeRoutePlanner(GridCellState.Obstacle));
            // TODO: Add a third planner (without any constraints)?

            this.primaryRoutePlanner = compositeRoutePlanner;

            // TODO: Replace with hardcoded route planner
            this.queueRoutePlanner = new SingleWaypointRoutePlanner();
        }

        //
        // Properties
        //

        // public AbstractGridLayoutProvider PrimaryGridLayoutProvider
        // {
        //     get {
        //         return primaryGridLayoutProvider;
        //     }
        // }

        // public AbstractGridLayoutProvider QueueGridLayoutProvider
        // {
        //     get {
        //         return queueGridLayoutProvider;
        //     }
        // }

        public IRoutePlanner PrimaryRoutePlanner
        {
            get {
                return primaryRoutePlanner;
            }
        }

        public IRoutePlanner QueueRoutePlanner
        {
            get {
                return queueRoutePlanner;
            }
        }

        public AbstractDiscreteTimeKeeper DiscreteTimeKeeper
        {
            get {
                return discreteTimeKeeper;
            }
        }

        public VehicleFactory VehicleFactory
        {
            get {
                return vehicleFactory;
            }
        }
    }
}