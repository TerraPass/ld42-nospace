using System.Collections.Generic;
using UnityEngine;

using RawGridLayout = NoSpace.Gameplay.Grid.Layout.RawGridLayout;

namespace NoSpace.Scripts.Gameplay
{
    public struct LevelScenario
    {
        public RawGridLayout       Layout          {get;}
        public List<Vector2Int>    VehicleSequence {get;}

        public LevelScenario(
            RawGridLayout       layout,
            List<Vector2Int> vehicleSequence
        )
        {
            this.Layout          = layout;
            this.VehicleSequence = vehicleSequence;
        }
    }
}