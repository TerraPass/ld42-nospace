using UnityEngine;
using System;

using NoSpace.Gameplay.Vehicles;

namespace NoSpace.Gameplay
{
    public class DiscreteTickEventArgs : EventArgs
    {
        public AbstractDiscreteTimeKeeper TimeKeeper {get;}

        public DiscreteTickEventArgs(AbstractDiscreteTimeKeeper timeKeeper)
        {
            this.TimeKeeper = timeKeeper;
        }
    }

    public class VehicleReachedDestinationEventArgs : EventArgs
    {
        public AbstractVehicle Vehicle {get;}
        public Vector2Int Destination {get;}

        public VehicleReachedDestinationEventArgs(
            AbstractVehicle vehicle,
            Vector2Int destination
        )
        {
            this.Vehicle = vehicle;
            this.Destination = destination;
        }
    }
}