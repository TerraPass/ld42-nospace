using System;
using System.Collections.Generic;
using UnityEngine;

using Terrapass.Extensions.Unity;

using NoSpace.Ui;
using NoSpace.Audio;

using NoSpace.Gameplay.Vehicles;
using NoSpace.Gameplay.Grid;
using NoSpace.Gameplay.Player;

namespace NoSpace.Gameplay
{
    public class LevelDirector : MonoBehaviour
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private GameplayServiceLocator gameplayServices;

        [SerializeField]
        private AbstractGrid primaryGrid;
        [SerializeField]
        private AbstractGrid queueGrid;
        [SerializeField]
        private VehicleQueue vehicleQueue;

        [SerializeField]
        private VehicleController vehicleController;

        [SerializeField]
        private int minTicksBetweenSpawns = 3;
        [SerializeField]
        private int maxTicksBetweenSpawns = 7;
        [SerializeField]
        private float doubleSizeProbability = 0.25f;
        [SerializeField]
        private int maxSpawnsBetweenDoubleSize = 6;

        [SerializeField]
        private StartMenu startMenu;

        [SerializeField]
        private GameOverMenu gameOverMenu;

        [SerializeField]
        private MoneyPanel moneyPanel;

        [SerializeField]
        private MusicPlayer musicPlayer;

        // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!!!!!
        [SerializeField]
        private AudioSource coinSoundSource;

        [SerializeField]
        private AudioClip[] coinSounds;

        //
        // Fields
        //

        private List<AbstractVehicle> activeVehicles;

        private AbstractVehicle nextVehicle;
        private int nextSpawnDelay;

        private int lastSpawnTime;

        // TODO
        //private bool isDemoMode;

        private int acceptedVehiclesCount = 0;
        private int moneyMadeCount = 0;

        private int dollarsPerCell = 3;

        private int sinceLastDoubleSize;

        //
        // Unity methods
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            this.primaryGrid.Initialize(gameplayServices.PrimaryRoutePlanner);
            this.queueGrid.Initialize(gameplayServices.QueueRoutePlanner);

            gameplayServices.DiscreteTimeKeeper.DiscreteTick += this.OnDiscreteTick;

            Time.timeScale = 0.0f;

            this.activeVehicles = new List<AbstractVehicle>();

            this.nextSpawnDelay = 0;
            this.lastSpawnTime = 0;
            this.sinceLastDoubleSize = 0;
            this.nextVehicle = MakeNextVehicle(true);

            this.startMenu.Visible = true;
            this.moneyPanel.Hide();
            this.gameOverMenu.Hide();

            this.musicPlayer.Play();
        }

        void OnDestroy()
        {
            gameplayServices.DiscreteTimeKeeper.DiscreteTick -= this.OnDiscreteTick;
        }

        //
        // Methods
        //

        public void StartGame()
        {
            this.startMenu.Visible = false;
            this.moneyPanel.Show();

            Time.timeScale = 1.0f;

            this.musicPlayer.Stop();
        }

        public void RestartGame()
        {
            foreach (var vehicle in activeVehicles)
            {
                Destroy(vehicle.gameObject);
            }
            activeVehicles.Clear();

            vehicleQueue.RemoveDummyVehicle();
            vehicleQueue.ReleaseAllVehicles();

            primaryGrid.ResetCellStates();
            queueGrid.ResetCellStates();

            acceptedVehiclesCount = 0;
            moneyMadeCount = 0;

            Time.timeScale = 1.0f;

            gameOverMenu.Hide();
            moneyPanel.Show();

            this.sinceLastDoubleSize = 0;
            this.nextSpawnDelay = 0;
            this.lastSpawnTime = 0;
            this.nextVehicle = MakeNextVehicle(true);

            musicPlayer.Stop();
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void OnVehicleDispatched(Vector2Int dimensions)
        {
            this.vehicleQueue.RemoveDummyVehicle();
            this.vehicleController.PendingVehicle = this.vehicleQueue.Front; 

            this.moneyMadeCount += dollarsPerCell * (dimensions.x*dimensions.y);

            Audio.AudioUtils.PlayOneOf(coinSoundSource, coinSounds);
        }

        //
        // Detail methods
        //

        private void OnDiscreteTick(object sender, DiscreteTickEventArgs args)
        {
            // Check win conditions
            if (primaryGrid.CellStates[primaryGrid.EntranceCell] != GridCellState.Empty
                && TryWinLevel())
            {
                return;
            }

            // Check if queue-to-grid transition is possible
            if (vehicleQueue.CanPopVehicle() && primaryGrid.CanSpawnVehicle(vehicleQueue.Front.CurrentDimensions))
            {
                var vehicle = vehicleQueue.PopVehicle();
                vehicle.SpawnOnGrid(primaryGrid);
                vehicleQueue.InstallDummyVehicle(vehicle.CurrentDimensions);
                this.vehicleController.ControlledVehicle = vehicle;

                this.acceptedVehiclesCount++;
            }

            // Try to spawn a new vehicle or trigger game over, if unable
            if (args.TimeKeeper.DiscreteTime - lastSpawnTime > nextSpawnDelay/* && vehicleQueue.CanPushVehicle(new Vector2Int(1,1))*/)  // TODO: Don't assume 1x1 will work for other kinds
            {
                if(!vehicleQueue.CanPushVehicle(new Vector2Int(1,1)))    // See comment above
                {
                    LoseLevel();
                    return;
                }

                activeVehicles.Add(nextVehicle);

                vehicleQueue.PushVehicle(nextVehicle);
                // If this is the first spawned vehicle in the level,
                // make it controller's pending vehicle.
                if (vehicleQueue.Front == nextVehicle)
                {
                    this.vehicleController.PendingVehicle = nextVehicle;
                }

                nextVehicle = MakeNextVehicle(false);
                nextSpawnDelay = GetRandomDelay();

                lastSpawnTime = args.TimeKeeper.DiscreteTime;
            }
        }

        private void LoseLevel()
        {
            OnGameOver(CollectGameStats());
        }

        private bool TryWinLevel()
        {
            var stats = CollectGameStats();
            if (stats.EmptySpaces == 0)
            {
                OnGameOver(stats);

                return true;
            }

            return false;
        }

        private void OnGameOver(GameStats gameStats)
        {
            moneyPanel.Hide();

            this.vehicleController.ControlledVehicle = null;
            this.vehicleController.PendingVehicle = null;

            Time.timeScale = 0.0f;

            this.gameOverMenu.Show(
                gameStats
            );

            musicPlayer.Play();
        }

        private GameStats CollectGameStats()
        {
            int totalSpaces = 0;
            int emptySpaces = 0;
            for(int y = 0; y < primaryGrid.CellStates.Height; y++)
            {
                for(int x = 0; x < primaryGrid.CellStates.Width; x++)
                {
                    Vector2Int gridPosition = new Vector2Int(x, y);
                    if(primaryGrid.CellStates[gridPosition] != GridCellState.Obstacle
                        && gridPosition != primaryGrid.EntranceCell
                        && gridPosition != primaryGrid.EntranceCell + Vector2Int.right)
                    {
                        totalSpaces++;
                        if(primaryGrid.CellStates[x, y] == GridCellState.Empty)
                            emptySpaces++;
                    }
                }
            }

            return new GameStats(moneyMadeCount, acceptedVehiclesCount, totalSpaces, emptySpaces);
        }

        private AbstractVehicle MakeNextVehicle(bool forceSingleSize)
        {
            bool evenSmallVehicles = (sinceLastDoubleSize % 2 == 0);
            bool mustSpawnDoubleSize = !forceSingleSize && evenSmallVehicles && (sinceLastDoubleSize >= maxSpawnsBetweenDoubleSize
                || UnityEngine.Random.value < doubleSizeProbability);

            if (mustSpawnDoubleSize)
                sinceLastDoubleSize = 0;
            else
                sinceLastDoubleSize++;

            return gameplayServices.VehicleFactory.CreateVehicle<Vehicle>(
                mustSpawnDoubleSize ? 2 : 1
            );
        }

        private int GetRandomDelay()
        {
            return (int)(minTicksBetweenSpawns + (maxTicksBetweenSpawns - minTicksBetweenSpawns)*UnityEngine.Random.value);
        }

        //
        // Properties
        //

        public bool IsPaused
        {
            get {
                return Time.timeScale < 0.1f;
            }
        }

        public int MoneyMade
        {
            get {
                return this.moneyMadeCount;
            }
        }
    }
}