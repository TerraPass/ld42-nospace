namespace NoSpace.Gameplay
{
    public struct GameStats
    {
        public int MoneyMade {get;}
        public int CarsAccepted {get;}
        public int TotalSpaces {get;}
        public int EmptySpaces {get;}

        public GameStats(
            int moneyMade,
            int carsAccepted,
            int totalSpaces,
            int emptySpaces
        )
        {
            this.MoneyMade    = moneyMade;
            this.CarsAccepted = carsAccepted;
            this.TotalSpaces  = totalSpaces;
            this.EmptySpaces  = emptySpaces;
        }

        public float Efficiency
        {
            get {
                return 1.0f - ((float)EmptySpaces/(float)TotalSpaces);
            }
        }
    }
}