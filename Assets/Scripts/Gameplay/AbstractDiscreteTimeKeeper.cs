using System;
using UnityEngine;

namespace NoSpace.Gameplay
{
    public abstract class AbstractDiscreteTimeKeeper : MonoBehaviour
    {
        //
        // Events
        //

        public abstract event EventHandler<DiscreteTickEventArgs> DiscreteTick;

        //
        // Properties
        //

        public abstract int DiscreteTime {get;}
        public abstract float TickProgress {get;}
    }
}