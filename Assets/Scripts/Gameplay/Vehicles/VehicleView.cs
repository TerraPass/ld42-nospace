using UnityEngine;

using Terrapass.Extensions.Unity;

namespace NoSpace.Gameplay.Vehicles
{
    public class VehicleView : MonoBehaviour
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private GameplayServiceLocator gameplayServices;

        [SerializeField]
        private Vehicle vehicle;

        [SerializeField]
        private bool discreteMovement;

        [SerializeField]
        private Transform bodyTransform;

        //
        // Fields
        //

        private Quaternion desiredRotation;

        //
        // Unity methods
        //

        void Start()
        {
            // this.EnsureRequiredFieldsAreSetInEditor();

            // TODO: Subscribe to events

            this.desiredRotation = Quaternion.identity;
        }

        void Update()
        {
            if (vehicle.Grid == null)
                return;

            this.transform.position = vehicle.Grid.View.ToVisualPosition(vehicle.GridPosition);

            if (vehicle.State == VehicleState.InTransitMoving && vehicle.CurrentWaypoint.HasValue)
            {
                var visualDelta = vehicle.Grid.View.GetVisualDelta(vehicle.CurrentWaypoint.Value, vehicle.GridPosition);

                if (!discreteMovement)
                {
                    this.transform.position += gameplayServices.DiscreteTimeKeeper.TickProgress * visualDelta;
                }

                //this.bodyTransform.rotation = Quaternion.LookRotation(visualDelta, Vector3.up);
                desiredRotation = Quaternion.LookRotation(visualDelta, Vector3.up);
            }

            this.bodyTransform.rotation = Quaternion.Lerp(
                this.bodyTransform.rotation,
                desiredRotation,
                gameplayServices.DiscreteTimeKeeper.TickProgress
            );
        }

        //
        // Methods
        //

        public void Initialize(GameplayServiceLocator gameplayServices)
        {
            this.gameplayServices = gameplayServices;
        }
    }
}