namespace NoSpace.Gameplay.Vehicles
{
    public enum VehicleState
    {
        Invalid            = 0,
        Queued             = 1,
        Selected           = 2,
        InTransitMoving    = 3,
        InTransitWaiting   = 4,
        ReachedDestination = 5
    }
}