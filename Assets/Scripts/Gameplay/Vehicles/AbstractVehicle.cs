using System;
using UnityEngine;

using NoSpace.Gameplay.Grid;

namespace NoSpace.Gameplay.Vehicles
{
    public abstract class AbstractVehicle : MonoBehaviour
    {
        //
        // Events
        //

        public abstract event EventHandler<VehicleReachedDestinationEventArgs> ReachedDestination;

        //
        // Methods
        //

        public abstract void SpawnOnGrid(AbstractGrid grid);

        public abstract bool CanReachDestination(Vector2Int destination);
        public abstract void StartMovingToDestination(Vector2Int destination);

        //
        // Properties
        //

        //public abstract AbstractGrid Grid {get; set;}

        public abstract Vector2Int GridPosition {get;}
        public abstract Vector2Int DefaultDimensions {get;}
        public abstract Vector2Int CurrentDimensions {get;}
        public abstract VehicleState State {get;}
    }
}