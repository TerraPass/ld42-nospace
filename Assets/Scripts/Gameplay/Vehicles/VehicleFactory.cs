using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using NoSpace.Utils;

namespace NoSpace.Gameplay.Vehicles
{
    public class VehicleFactory : MonoBehaviour
    {
        //
        // Inspector types
        //

        [Serializable]
        private struct VehicleSizeMapping
        {
            public int size;
            public GameObject vehiclePrefab;
        }

        //
        // Inspector fields
        //

        [SerializeField]
        private GameplayServiceLocator gameplayServices;

        [SerializeField]
        private VehicleSizeMapping[] vehicleSizeMappings;

        [SerializeField]
        private Transform spawnLocation;

        //
        // Fields
        //

        private IDictionary<int, List<GameObject>> vehiclePrefabs;

        //
        // Unity methods
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            this.vehiclePrefabs = new Dictionary<int, List<GameObject>>();
            foreach (var mapping in this.vehicleSizeMappings)
            {
                if (!vehiclePrefabs.ContainsKey(mapping.size))
                    vehiclePrefabs.Add(mapping.size, new List<GameObject>());

                vehiclePrefabs[mapping.size].Add(mapping.vehiclePrefab);
            }
        }

        //
        // Methods
        //

        public GameObject CreateVehicle(int size)
        {
            if (!vehiclePrefabs.ContainsKey(size))
            {
                throw new InvalidOperationException(
                    string.Format("There are no vehicle prefabs of size {0}", size)
                );
            }

            var vehicleObject = Instantiate(
                CollectionUtils.GetRandomElement(vehiclePrefabs[size]),
                spawnLocation.position,
                Quaternion.identity // TODO: Use spawnLocation.rotation somehow
            );
            var vehicle       = vehicleObject.GetComponent<Vehicle>();
            var vehicleView   = vehicleObject.GetComponent<VehicleView>();

            DebugUtils.Assert(vehicle != null, "Vehicle prefab must contain an AbstractVehicle component");
            DebugUtils.Assert(vehicleView != null, "Vehicle prefab must contain a VehicleView component");

            vehicle.Initialize(gameplayServices);
            vehicleView.Initialize(gameplayServices);

            return vehicleObject;
        }

        public T CreateVehicle<T>(int size)
        {
            var vehicleObject = CreateVehicle(size);
            return vehicleObject.GetComponent<T>();
        }
    }
}