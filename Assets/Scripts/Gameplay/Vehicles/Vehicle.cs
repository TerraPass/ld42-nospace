using System;
using System.Collections.Generic;
using UnityEngine;

using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using NoSpace.Gameplay.Grid.Routing;

#if UNITY_EDITOR
using UnityEditor;
#endif

using NoSpace.Gameplay.Grid;

namespace NoSpace.Gameplay.Vehicles
{
    public class Vehicle : AbstractVehicle
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private GameplayServiceLocator gameplayServices;

        // [SerializeField]
        // private AbstractGrid grid;

        [SerializeField]
        private Vector2Int initialDimensions;

        //
        // Fields
        //

        private AbstractGrid grid;
        private AbstractDiscreteTimeKeeper timeKeeper;

        private Vector2Int gridPosition;

        private Vector2Int defaultDimensions;
        private Vector2Int currentDimensions;

        private VehicleState state;

        private Vector2Int? destination;
        private LinkedList<Vector2Int> currentWaypoints;

        private Vector2Int? cachedPotentialDestination;
        private bool? cachedCanReachDestination;

        //
        // Unity methods
        //

        void Start()
        {
            // this.EnsureRequiredFieldsAreSetInEditor();

            this.timeKeeper = gameplayServices.DiscreteTimeKeeper;
            timeKeeper.DiscreteTick += this.OnDiscreteTick;

            this.defaultDimensions = initialDimensions;
            this.currentDimensions = defaultDimensions;

            this.state = VehicleState.Invalid;
        }

        void OnDestroy()
        {
            timeKeeper.DiscreteTick -= this.OnDiscreteTick;

            if (grid != null)
                grid.OnVehicleDespawned(this);
        }

        void FixedUpdate()
        {
            //Debug.LogFormat("Discrete time: {0} ({1})", timeKeeper.DiscreteTime, timeKeeper.TickProgress);
        }

        //
        // Methods
        //

        public void Initialize(GameplayServiceLocator gameplayServices)
        {
            this.gameplayServices = gameplayServices;
        }

        public override void SpawnOnGrid(AbstractGrid grid)
        {
            DebugUtils.Assert(grid.CanStartMoving(this, grid.EntranceCell), "Grid must be ready to accept spawned vehicle");

            if (this.grid != null)
            {
                this.grid.OnVehicleDespawned(this);
            }

            this.grid = grid;
            this.gridPosition = grid.EntranceCell;
            grid.OnVehicleSpawned(this);

            this.state = VehicleState.Selected;
        }

        public override bool CanReachDestination(Vector2Int destination)
        {
            if (cachedPotentialDestination.HasValue && cachedCanReachDestination.HasValue
                && cachedPotentialDestination.Value == destination)
            {
                return cachedCanReachDestination.Value;
            }

            // TODO: Cache the potential route, since chances are it will be used in StartMovingToDestination()
            var potentialRoute = PlanRoute(destination);
            cachedPotentialDestination = destination;
            cachedCanReachDestination = potentialRoute.IsValid;
            return potentialRoute.IsValid;
        }

        public override void StartMovingToDestination(Vector2Int destination)
        {
            var route = PlanRoute(destination);
            if (!route.IsValid)
            {
                throw new InvalidOperationException("Can't find the route to destiation");
            }

            this.destination = destination;
            currentWaypoints = route.waypoints;

            this.state = VehicleState.InTransitWaiting;
        }

        //
        // Event handlers
        //

        void OnDiscreteTick(object sender, DiscreteTickEventArgs args)
        {
            // Invalidate cached decision on destination reachability
            cachedPotentialDestination = null;
            cachedCanReachDestination = null;

            if(state == VehicleState.InTransitMoving)
            {
                DebugUtils.Assert(destination.HasValue, "must have destination");
                DebugUtils.Assert(currentWaypoints != null && currentWaypoints.Count > 0, "must have waypoints");

                var waypoint = currentWaypoints.First.Value;
                currentWaypoints.RemoveFirst();

                grid.OnVehicleMoved(this, gridPosition, waypoint);
                this.gridPosition = waypoint;

                if (this.gridPosition == destination)
                {
                    state = VehicleState.ReachedDestination;
                    grid.OnVehicleReachedDestination(this);

                    if (this.ReachedDestination != null)
                        this.ReachedDestination(this, new VehicleReachedDestinationEventArgs(this, destination.Value));
                }
                else
                {
                    state = VehicleState.InTransitWaiting;
                    // Will process what to do next on this same tick
                }
            }

            if(state == VehicleState.InTransitWaiting)
            {
                DebugUtils.Assert(destination.HasValue, "must have destination");
                DebugUtils.Assert(currentWaypoints != null && currentWaypoints.Count > 0, "must have waypoints");

                var waypoint = currentWaypoints.First.Value;

                if (this.grid.CanStartMoving(this, waypoint))
                {
                    state = VehicleState.InTransitMoving;
                    grid.OnVehicleStartedMoving(this, gridPosition, waypoint);
                }
                else
                {
                    var route = PlanRoute(destination.Value);
                    if (route.IsValid)
                        currentWaypoints = route.waypoints;
                }
            }
        }

        //
        // Detail methods
        //

        private Route PlanRoute(Vector2Int destination)
        {
            return this.grid.RoutePlanner.PlanRoute(
                this.grid.CellStates,
                this.gridPosition,
                destination,
                this.currentDimensions
            );
        }

        //
        // Properties
        //

        public override Vector2Int GridPosition
        {
            get
            {
                return gridPosition;
            }
        }

        public override Vector2Int DefaultDimensions
        {
            get {
                return defaultDimensions;
            }
        }

        public override Vector2Int CurrentDimensions
        {
            get {
                return currentDimensions;
            }
        }

        public override VehicleState State
        {
            get {
                return state;
            }
        }

        public AbstractGrid Grid
        {
            get {
                return grid;
            }
        }

        public Vector2Int? CurrentWaypoint
        {
            get {
                return (currentWaypoints != null && currentWaypoints.Count > 0)
                    ? new System.Nullable<Vector2Int>(currentWaypoints.First.Value)
                    : null;
            }
        }

        public Vector2Int? SecondWaypoint
        {
            get {
                if (currentWaypoints == null || currentWaypoints.Count < 2)
                {
                    return null;
                }

                var enumerator = currentWaypoints.GetEnumerator();
                enumerator.MoveNext();
                enumerator.MoveNext();
                return enumerator.Current;
            }
        }

        //
        // Events
        //

        public override event EventHandler<VehicleReachedDestinationEventArgs> ReachedDestination;

        //
        // Debug methods
        //

        void OnDrawGizmos()
        {
            if (!Application.isPlaying)
                return;

            #if UNITY_EDITOR
            Handles.color = Color.cyan;
            
            Handles.Label(this.transform.position, state.ToString());

            if (currentWaypoints == null)
                return;

            Vector2Int? lastWaypoint = null;
            foreach (var waypoint in currentWaypoints)
            {
                UnityEngine.Debug.DrawLine(
                    grid.View.ToVisualPositionCentered(lastWaypoint == null ? gridPosition : lastWaypoint.Value),
                    grid.View.ToVisualPositionCentered(waypoint)
                );

                lastWaypoint = waypoint;
            }
            #endif
        }
    }
}