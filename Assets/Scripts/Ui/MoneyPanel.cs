using System;
using UnityEngine;
using UnityEngine.UI;

using NoSpace.Gameplay;

namespace NoSpace.Ui
{
    public class MoneyPanel : MonoBehaviour
    {
        [SerializeField]
        private LevelDirector levelDirector;

        [SerializeField]
        private Text moneyText;

        public void Show()
        {
            this.gameObject.SetActive(true);
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
        }

        void Update()
        {
            moneyText.text = string.Format("${0}", levelDirector.MoneyMade);
        }
    }
}