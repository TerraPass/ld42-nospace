using System;
using UnityEngine;
using UnityEngine.UI;

using NoSpace.Gameplay;

namespace NoSpace.Ui
{
    public class GameOverMenu : MonoBehaviour
    {
        private const string GameOverText = "Game Over";
        private const string WellDoneText = "Well Done";

        private const string GameOverReason = "The line got too long";
        private const string WellDoneReason = "You packed the lot full of cars";

        private const string MoneyTextTemplate      = "Money made: ${0}";
        private const string CarsTextTemplate       = "Cars accepted: {0}";
        private const string SpaceTextTemplate      = "Empty spaces left: {0}/{1}";
        private const string EfficiencyTextTemplate = "Efficiency: {0}%";

        [SerializeField]
        private LevelDirector levelDirector;

        [SerializeField]
        private Text titleText;

        [SerializeField]
        private Text reasonText;

        [SerializeField]
        private Text moneyText;

        [SerializeField]
        private Text carsText;

        [SerializeField]
        private Text spaceText;

        [SerializeField]
        private Text efficiencyText;

        [SerializeField]
        private Button quitButton;

        [SerializeField]
        private Button playAgainButton;

        void Start()
        {
            quitButton.onClick.AddListener(
                () => levelDirector.QuitGame()
            );
            playAgainButton.onClick.AddListener(
                () => levelDirector.RestartGame()
            );

            this.gameObject.SetActive(false);
        }

        public void Show(GameStats stats)
        {
            moneyText.text = string.Format(MoneyTextTemplate, stats.MoneyMade);
            carsText.text  = string.Format(CarsTextTemplate, stats.CarsAccepted);
            spaceText.text = string.Format(SpaceTextTemplate, stats.EmptySpaces, stats.TotalSpaces);
            efficiencyText.text = string.Format(EfficiencyTextTemplate, (int)(stats.Efficiency * 100.0f));

            titleText.text  = stats.EmptySpaces <= 0 ? WellDoneText : GameOverText;
            reasonText.text = stats.EmptySpaces <= 0 ? WellDoneReason: GameOverReason;

            this.gameObject.SetActive(true);
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
        }
    }
}