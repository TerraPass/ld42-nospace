using UnityEngine;
using UnityEngine.UI;

using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using NoSpace.Gameplay;

namespace NoSpace.Ui
{
    public class StartMenu : MonoBehaviour
    {
        //
        // Inspector fields
        //

        [SerializeField]
        private LevelDirector levelDirector;

        [SerializeField]
        private Button startGameButton;

        [SerializeField]
        private Button howToPlayButton;

        [SerializeField]
        private Button closeHowToPlayButton;

        [SerializeField]
        private GameObject howToPlayPanel;

        //
        // Unity methods
        //

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            startGameButton.onClick.AddListener(
                () => levelDirector.StartGame()
            );

            howToPlayButton.onClick.AddListener(
                () => this.OnHowToPlayButtonClicked()
            );

            closeHowToPlayButton.onClick.AddListener(
                () => this.OnCloseHowToPlayButtonClicked()
            );

            this.howToPlayPanel.SetActive(false);
        }

        //
        // Detail methods
        //

        void OnHowToPlayButtonClicked()
        {
            this.Visible = false;

            this.howToPlayPanel.SetActive(true);
        }

        void OnCloseHowToPlayButtonClicked()
        {
            this.Visible = true;

            this.howToPlayPanel.SetActive(false);
        }

        //
        // Properties
        //

        public bool Visible
        {
            get {
                return this.gameObject.activeSelf;
            }
            set {
                this.gameObject.SetActive(value);
            }
        }
    }
}